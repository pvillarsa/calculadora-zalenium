package CalculadoraTestNG.calculadora;

import org.testng.TestListenerAdapter;
import org.testng.TestNG;

public class Main {

	public static void main(String[] args) {

		TestListenerAdapter tt = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { CalculadoraTestNG.calculadora.Demo.class});
		testng.addListener(tt);
		testng.run();
	}
}

