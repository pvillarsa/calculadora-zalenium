package CalculadoraTestNG.calculadora;

import java.io.IOException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;


public class Demo {

	@Test
	public static void TestCal() throws IOException {

		DesiredCapabilities dc = null;
		dc = DesiredCapabilities.chrome();
		WebDriver driver = new RemoteWebDriver(new URL("http://34.70.141.26:4444/wd/hub"), dc);
		driver.get("http://34.96.111.70/");
		boolean returnTest = false;
		boolean returnTest2 = false;
		boolean returnTest3 = false;
		boolean result = false;

		try {
			driver.findElement(By.id("btn_reiniciar")).click();
			driver.findElement(By.id("btn_n2")).click();
			driver.findElement(By.id("btn_mas")).click();
			driver.findElement(By.id("btn_n3")).click();
			driver.findElement(By.id("btn_resultado")).click();
			returnTest = "5".equals(driver.findElement(By.id("entrada")).getAttribute("value"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}

		

		// Test Suma2
		try {
			driver.findElement(By.id("btn_reiniciar")).click();
			driver.findElement(By.id("btn_n5")).click();
			driver.findElement(By.id("btn_n8")).click();
			driver.findElement(By.id("btn_mas")).click();
			driver.findElement(By.id("btn_n9")).click();
			driver.findElement(By.id("btn_resultado")).click();

			returnTest2 = "67".equals(driver.findElement(By.id("entrada")).getAttribute("value"));
		} catch (Exception e) {
			System.out.println(e.toString());
		}
		
		WebDriverWait wai = new WebDriverWait(driver, 90);

		// Test Resta3
		try {
			driver.findElement(By.id("btn_reiniciar")).click();
			driver.findElement(By.id("btn_n5")).click();
			driver.findElement(By.id("btn_n8")).click();
			driver.findElement(By.id("btn_menos")).click();
			driver.findElement(By.id("btn_n9")).click();
			driver.findElement(By.id("btn_resultado")).click();
			returnTest3 = "49".equals(driver.findElement(By.id("entrada")).getAttribute("value"));

		} catch (Exception e) {
			System.out.println(e.toString());
		}
	
		result = returnTest && returnTest2 && returnTest3;
		
		System.out.println("*************************");
		System.out.println("Resultado:");
		
		System.out.println("Test N° 1 = " + (returnTest ? "Test Aprobado " : "Test Falló"));
		
		System.out.println("Test N° 2 = " + (returnTest2 ? "Test Aprobado" : "Test Falló"));
		
		System.out.println("Test N° 3 = " + (returnTest3 ? "Test Aprobado" : "Test Falló"));
			
		driver.close();
		driver.quit();
		System.out.println("-------------------------------");
    	System.out.println("Tests: " + (result ? "OK" : "NOK" ));
		System.exit(result ? 0 : -1); 
	}

}
